if exist vendor\bin\premake\premake5.exe (
	call vendor\bin\premake\premake5.exe vs2019
) else (
	if not exist "vendor\bin\premake\" mkdir vendor\bin\premake\
	powershell.exe Invoke-WebRequest https://github.com/premake/premake-core/releases/download/v5.0.0-beta1/premake-5.0.0-beta1-windows.zip -OutFile vendor\bin\premake\premake5.zip
	powershell.exe Expand-Archive vendor\bin\premake\premake5.zip -DestinationPath vendor\bin\premake\
	call vendor\bin\premake\premake5.exe vs2019
)
PAUSE