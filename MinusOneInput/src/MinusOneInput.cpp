#include "EditorLayer.h"

class MinusOneInutApplication : public MinusOne::Application
{
public:
	MinusOneInutApplication()
	{
		PushLayer(new MinusOne::EditorLayer());
	}

};

MinusOne::Application* MinusOne::CreateApplication()
{
	return new MinusOneInutApplication();
}