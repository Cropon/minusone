MinusOne is a kind of "game engine" built for fun.
Currently has an OpenGL implementation in the main branch with a scene view, imgui UI and simple 3D renderer.
The VulkanImplementation branch is under development and is about to recreate all of the above using the Vulkan API.

Special thanks to Cherno and his Engine development series!
