#pragma once

//since stbi gets internal compiler errors
#define STBI_NO_SIMD

#ifdef MO_PLATFORM_WINDOWS
#include <Windows.h>
#endif

#include <iostream>
#include <memory>
#include <utility>
#include <algorithm>
#include <functional>

#include <fstream>
#include <array>
#include <string>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <cmath>
#include "MinusOne/Log.h"