#pragma once

#include "MinusOne/Application.h"
#include "MinusOne/Log.h"
#include "MinusOne/Core/TimeStep.h"
#include "MinusOne/Events/Event.h"
#include "MinusOne/Events/ApplicationEvent.h"
#include "MinusOne/Events/KeyEvent.h"
#include "MinusOne/Events/MouseEvent.h"
#include "imgui/imgui.h"
#include "MinusOne/ObjectsSystem/MinusOneObject.h"
#include "MinusOne/ObjectsSystem/Scene.h"
#include "MinusOne/ObjectsSystem/RenderComponent.h"
#include "MinusOne/ObjectsSystem/Transform.h"
#include "MinusOne/Gizmo/GizmoLayer.h"

// --- MinusOne Render API ------------------------------
#include "MinusOne/Renderer/Renderer.h"
#include "MinusOne/Renderer/Framebuffer.h"
#include "MinusOne/Renderer/Buffer.h"
#include "MinusOne/Renderer/VertexArray.h"
#include "MinusOne/Renderer/Texture.h"
#include "MinusOne/Renderer/Shader.h"
#include "MinusOne/Renderer/Mesh.h"
#include "MinusOne/Renderer/Camera.h"
#include "MinusOne/Renderer/Material.h"
// ---------------------------------------------------