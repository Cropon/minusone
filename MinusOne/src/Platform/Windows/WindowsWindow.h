#pragma once

#include "MinusOne/Window.h"
#include "MinusOne/Renderer/GraphicsContext.h"

#include <GLFW/glfw3.h>

namespace MinusOne {

	class WindowsWindow : public Window
	{
	public:
		WindowsWindow(const WindowProps& props);
		virtual ~WindowsWindow();

		void OnUpdate() override;

		inline unsigned int GetWidth() const override { return m_Data.Width; }
		inline unsigned int GetHeight() const override { return m_Data.Height; }
		inline int GetPositionX() const override { return m_Data.XPos; }
		inline int GetPositionY() const override { return m_Data.YPos; }

		// Window attributes
		inline void SetEventCallback(const EventCallbackFn& callback) override { m_Data.EventCallback = callback; }
		void SetVSync(bool enabled) override;
		bool IsVSync() const override;
	private:
		virtual void Init(const WindowProps& props);
		virtual void Shutdown();
		inline virtual void* GetNativeWindow() const { return m_Window; }
	private:
		GLFWwindow* m_Window;
		GraphicsContext* m_Context;

		struct WindowData
		{
			std::string Title;
			unsigned int Width, Height;
			int XPos, YPos;
			bool VSync;

			EventCallbackFn EventCallback;
		};

		WindowData m_Data;
	};

}