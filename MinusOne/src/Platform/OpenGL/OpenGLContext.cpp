#include "mopch.h"
#include "OpenGLContext.h"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

namespace MinusOne {
	OpenGLContext::OpenGLContext(GLFWwindow* windowHandle)
		: m_WindowHandle(windowHandle)
	{
		MO_CORE_ASSERT(windowHandle, "Window handle is null");
	}

	void OpenGLContext::Init()
	{
		glfwMakeContextCurrent(m_WindowHandle);
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		MO_CORE_ASSERT(status, "Failed to initialize Glad!");

		MO_CORE_INFO("OpenGL Info:");
		MO_CORE_INFO("  Vendor: {0}", glGetString(GL_VENDOR));
		MO_CORE_INFO("  Renderer: {0}", glGetString(GL_RENDERER));
		MO_CORE_INFO("  Version: {0}", glGetString(GL_VERSION));
	}

	void OpenGLContext::SwapBuffers()
	{
		glfwSwapBuffers(m_WindowHandle);
	}
}