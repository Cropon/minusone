#pragma once

#include <glm/gtx/transform.hpp>

namespace MinusOne {

	class Transform
	{
	public:
		Transform();
		~Transform() = default;

		virtual void OnImGuiRender();

		glm::mat4 GetModelMatrix();

		glm::vec3 GetPosition() { return m_Position; }
		glm::vec3 GetRotation() { return m_Rotation; }
		glm::vec3 GetScale() { return m_Scale; }
		void SetPosition(glm::vec3 position) { m_Position.x = position.x; m_Position.y = position.y; m_Position.z = position.z; }
		void SetRotation(glm::vec3& rotation) { m_Rotation.x = rotation.x; m_Rotation.y = rotation.y; m_Rotation.z = rotation.z; }
		void SetScale(glm::vec3& scale) { m_Scale.x = scale.x; m_Scale.y = scale.y; m_Scale.z = scale.z; }

	private:
		glm::vec3 m_Position;
		glm::vec3 m_Rotation;
		glm::vec3 m_Scale;
	};

}