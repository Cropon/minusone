#include "mopch.h"
#include "RenderComponent.h"
#include "Scene.h"
#include "glm/glm.hpp"
#include "MinusOne/Application.h"
#include "imgui.h"

namespace MinusOne {

	void RenderComponent::Start() {}

	void RenderComponent::OnUpdate(Timestep ts) 
	{
		m_Material->Set("u_AlbedoColor", m_AlbedoInput.Color);
		m_Material->Set("u_Metalness", m_MetalnessInput.Value);
		m_Material->Set("u_Roughness", m_RoughnessInput.Value);
		m_Material->Set("u_ViewProjectionMatrix", Scene::GetCurrentActiveScene()->GetCamera().GetViewProjectionMatrix());
		m_Material->Set("u_ModelMatrix", m_Object->GetTransform().GetModelMatrix());
		m_Material->Set("lights", Scene::GetCurrentActiveScene()->GetLight());
		m_Material->Set("u_CameraPosition", Scene::GetCurrentActiveScene()->GetCamera().GetPosition());
		m_Material->Set("u_RadiancePrefilter", Scene::GetCurrentActiveScene()->GetRadiancePrefilterUsage() ? 1.0f : 0.0f);
		m_Material->Set("u_AlbedoTexToggle", m_AlbedoInput.UseTexture ? 1.0f : 0.0f);
		m_Material->Set("u_NormalTexToggle", m_NormalInput.UseTexture ? 1.0f : 0.0f);
		m_Material->Set("u_MetalnessTexToggle", m_MetalnessInput.UseTexture ? 1.0f : 0.0f);
		m_Material->Set("u_RoughnessTexToggle", m_RoughnessInput.UseTexture ? 1.0f : 0.0f);
		m_Material->Set("u_EnvMapRotation", Scene::GetCurrentActiveScene()->GetEnvMapRotation());
		
		m_Material->Set("u_EnvRadianceTex", Scene::GetCurrentActiveScene()->GetEnvironmentCubeMap());
		m_Material->Set("u_EnvIrradianceTex", Scene::GetCurrentActiveScene()->GetEnvironmentIrradiance());
		m_Material->Set("u_BRDFLUTTexture", Scene::GetCurrentActiveScene()->GetLutTexture());

		if (m_AlbedoInput.TextureMap)
			m_Material->Set("u_AlbedoTexture", m_AlbedoInput.TextureMap);
		if (m_NormalInput.TextureMap)
			m_Material->Set("u_NormalTexture", m_NormalInput.TextureMap);
		if (m_MetalnessInput.TextureMap)
			m_Material->Set("u_MetalnessTexture", m_MetalnessInput.TextureMap);
		if (m_RoughnessInput.TextureMap)
			m_Material->Set("u_RoughnessTexture", m_RoughnessInput.TextureMap);

		m_Mesh->Render(ts, glm::scale(glm::mat4(1.0f), glm::vec3(1)), m_MeshMaterial);
	}

	void RenderComponent::OnEnd() {}
	void RenderComponent::OnImguiRender()
	{
#ifdef MINUSONE_EDITOR
		if (ImGui::TreeNode("Mesh"))
		{
			std::string fullpath = m_Mesh ? m_Mesh->GetFilePath() : "None";
			size_t found = fullpath.find_last_of("/\\");
			std::string path = found != std::string::npos ? fullpath.substr(found + 1) : fullpath;
			ImGui::Text(path.c_str()); ImGui::SameLine();
			if (ImGui::Button("...##Mesh"))
			{
				std::string filename = Application::Get().OpenFile("");
				if (filename != "")
				{
					m_Mesh.reset(new Mesh(filename, m_MeshShader, m_Material));
					m_MeshMaterial.reset(new MaterialInstance(m_Mesh->GetMaterial()));
				}
			}

			ImGui::TreePop();
		}

		if (ImGui::TreeNode("Material"))
		{
			ImGui::PushItemWidth(-1);
			// Textures ------------------------------------------------------------------------------
			{
				// Albedo
				if (ImGui::CollapsingHeader("Albedo", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
				{
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
					ImGui::Image(m_AlbedoInput.TextureMap ? (void*)m_AlbedoInput.TextureMap->GetRendererID() : (void*)Scene::GetCurrentActiveScene()->GetFallbackTexture()->GetRendererID(), ImVec2(64, 64));
					ImGui::PopStyleVar();
					if (ImGui::IsItemHovered())
					{
						if (m_AlbedoInput.TextureMap)
						{
							ImGui::BeginTooltip();
							ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
							ImGui::TextUnformatted(m_AlbedoInput.TextureMap->GetPath().c_str());
							ImGui::PopTextWrapPos();
							ImGui::Image((void*)m_AlbedoInput.TextureMap->GetRendererID(), ImVec2(384, 384));
							ImGui::EndTooltip();
						}
						if (ImGui::IsItemClicked())
						{
							std::string filename = Application::Get().OpenFile("");
							if (filename != "")
								m_AlbedoInput.TextureMap.reset(Texture2D::Create(filename, m_AlbedoInput.SRGB));
						}
					}
					ImGui::SameLine();
					ImGui::BeginGroup();
					ImGui::Checkbox("Use##AlbedoMap", &m_AlbedoInput.UseTexture);
					if (ImGui::Checkbox("sRGB##AlbedoMap", &m_AlbedoInput.SRGB))
					{
						if (m_AlbedoInput.TextureMap)
							m_AlbedoInput.TextureMap.reset(Texture2D::Create(m_AlbedoInput.TextureMap->GetPath(), m_AlbedoInput.SRGB));
					}
					ImGui::EndGroup();
					ImGui::SameLine();
					ImGui::ColorEdit3("Color##Albedo", glm::value_ptr(m_AlbedoInput.Color), ImGuiColorEditFlags_NoInputs);
				}
			}
			{
				// Normals
				if (ImGui::CollapsingHeader("Normals", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
				{
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
					ImGui::Image(m_NormalInput.TextureMap ? (void*)m_NormalInput.TextureMap->GetRendererID() : (void*)Scene::GetCurrentActiveScene()->GetFallbackTexture()->GetRendererID(), ImVec2(64, 64));
					ImGui::PopStyleVar();
					if (ImGui::IsItemHovered())
					{
						if (m_NormalInput.TextureMap)
						{
							ImGui::BeginTooltip();
							ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
							ImGui::TextUnformatted(m_NormalInput.TextureMap->GetPath().c_str());
							ImGui::PopTextWrapPos();
							ImGui::Image((void*)m_NormalInput.TextureMap->GetRendererID(), ImVec2(384, 384));
							ImGui::EndTooltip();
						}
						if (ImGui::IsItemClicked())
						{
							std::string filename = Application::Get().OpenFile("");
							if (filename != "")
								m_NormalInput.TextureMap.reset(Texture2D::Create(filename));
						}
					}
					ImGui::SameLine();
					ImGui::Checkbox("Use##NormalMap", &m_NormalInput.UseTexture);
				}
			}
			{
				// Metalness
				if (ImGui::CollapsingHeader("Metalness", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
				{
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
					ImGui::Image(m_MetalnessInput.TextureMap ? (void*)m_MetalnessInput.TextureMap->GetRendererID() : (void*)Scene::GetCurrentActiveScene()->GetFallbackTexture()->GetRendererID(), ImVec2(64, 64));
					ImGui::PopStyleVar();
					if (ImGui::IsItemHovered())
					{
						if (m_MetalnessInput.TextureMap)
						{
							ImGui::BeginTooltip();
							ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
							ImGui::TextUnformatted(m_MetalnessInput.TextureMap->GetPath().c_str());
							ImGui::PopTextWrapPos();
							ImGui::Image((void*)m_MetalnessInput.TextureMap->GetRendererID(), ImVec2(384, 384));
							ImGui::EndTooltip();
						}
						if (ImGui::IsItemClicked())
						{
							std::string filename = Application::Get().OpenFile("");
							if (filename != "")
								m_MetalnessInput.TextureMap.reset(Texture2D::Create(filename));
						}
					}
					ImGui::SameLine();
					ImGui::Checkbox("Use##MetalnessMap", &m_MetalnessInput.UseTexture);
					ImGui::SameLine();
					ImGui::SliderFloat("Value##MetalnessInput", &m_MetalnessInput.Value, 0.0f, 1.0f);
				}
			}
			{
				// Roughness
				if (ImGui::CollapsingHeader("Roughness", nullptr, ImGuiTreeNodeFlags_DefaultOpen))
				{
					ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(10, 10));
					ImGui::Image(m_RoughnessInput.TextureMap ? (void*)m_RoughnessInput.TextureMap->GetRendererID() : (void*)Scene::GetCurrentActiveScene()->GetFallbackTexture()->GetRendererID(), ImVec2(64, 64));
					ImGui::PopStyleVar();
					if (ImGui::IsItemHovered())
					{
						if (m_RoughnessInput.TextureMap)
						{
							ImGui::BeginTooltip();
							ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
							ImGui::TextUnformatted(m_RoughnessInput.TextureMap->GetPath().c_str());
							ImGui::PopTextWrapPos();
							ImGui::Image((void*)m_RoughnessInput.TextureMap->GetRendererID(), ImVec2(384, 384));
							ImGui::EndTooltip();
						}
						if (ImGui::IsItemClicked())
						{
							std::string filename = Application::Get().OpenFile("");
							if (filename != "")
								m_RoughnessInput.TextureMap.reset(Texture2D::Create(filename));
						}
					}
					ImGui::SameLine();
					ImGui::Checkbox("Use##RoughnessMap", &m_RoughnessInput.UseTexture);
					ImGui::SameLine();
					ImGui::SliderFloat("Value##RoughnessInput", &m_RoughnessInput.Value, 0.0f, 1.0f);
				}
			}
			ImGui::TreePop();
		}


		if (ImGui::TreeNode("Shaders"))
		{
			auto& shaders = Shader::s_AllShaders;
			for (auto& shader : shaders)
			{
				if (ImGui::TreeNode(shader->GetName().c_str()))
				{
					std::string buttonName = "Reload##" + shader->GetName();
					if (ImGui::Button(buttonName.c_str()))
						shader->Reload();
					ImGui::TreePop();
				}
			}
			ImGui::TreePop();
		}
#endif

	}

}