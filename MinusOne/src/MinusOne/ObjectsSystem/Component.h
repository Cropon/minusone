#pragma once

#include "MinusOne/Core/Timestep.h"

namespace MinusOne {

	class Component 
	{
	public:
		virtual void Start() = 0;
		virtual void OnUpdate(Timestep ts) = 0;
		virtual void OnEnd() = 0;
		virtual void OnImguiRender() = 0;
	};

}