#include "mopch.h"
#include "Scene.h"
#include "imgui.h"
#include "MinusOne/Utils.h"

namespace MinusOne {

	Scene* Scene::m_CurrentlyActiveScene = nullptr;

	void Scene::PushObject(MinusOneObject* sceneObj) 
	{
		m_Objects.push_back(sceneObj);
	}

	void Scene::Start() 
	{
		// Environment
		m_EnvironmentCubeMap.reset(TextureCube::Create("assets/textures/environments/Arches_E_PineTree_Radiance.tga"));
		//m_EnvironmentCubeMap.reset(TextureCube::Create("assets/textures/environments/DebugCubeMap.tga"));
		m_EnvironmentIrradiance.reset(TextureCube::Create("assets/textures/environments/Arches_E_PineTree_Irradiance.tga"));
		m_BRDFLUT.reset(Texture2D::Create("assets/textures/BRDF_LUT.tga"));
		for (int i = 0; i < m_Objects.size(); ++i)
		{
			//TODO: come up with proper IDs
			m_Objects[i]->SetID(i);
			m_Objects[i]->Start();
		}
	}

	void Scene::OnUpdate(Timestep ts) 
	{
		m_Camera.Update(ts);
		m_EnvironmentIrradiance->Bind(0);
		for each (auto obj in m_Objects)
		{
			obj->OnUpdate(ts);
		}
	}

	void Scene::OnEnd()
	{
		for each (auto obj in m_Objects)
		{
			obj->OnEnd();
		}
	}

	void Scene::OnImguiRender()
	{
#ifdef MINUSONE_EDITOR
		ImGui::Begin("Scene properties");
		ImGuiHelper::Property("Light Direction", m_Light.Direction, PropertyFlag::None);
		ImGuiHelper::Property("Light Radiance", m_Light.Radiance, PropertyFlag::ColorProperty);
		ImGuiHelper::Property("Light Multiplier", m_LightMultiplier, 0.0f, 5.0f, PropertyFlag::None);
		ImGuiHelper::Property("Exposure", m_Exposure, 0.0f, 5.0f, PropertyFlag::None);
		ImGuiHelper::Property("Radiance Prefiltering", m_RadiancePrefilter);
		ImGuiHelper::Property("Env Map Rotation", m_EnvMapRotation, -360.0f, 360.0f, PropertyFlag::None);

		ImGui::End();

		ImGui::Begin("Scene view");
		
		const char* items[200];
		for (int i = 0; i < m_Objects.size(); ++i) 
		{
			items[i] = m_Objects[i]->GetName().c_str();
		}

		ImGui::PushItemWidth(-1);
		ImGui::ListBox("", &m_CurrentlyChosen, items, m_Objects.size(), m_Objects.size());

		ImGui::End();

		m_Objects[m_CurrentlyChosen]->OnImGuiRender();

#endif // MINUSONE_EDITOR
	}
}