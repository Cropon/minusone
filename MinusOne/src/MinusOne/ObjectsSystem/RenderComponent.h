#pragma once

#include "MinusOne/Renderer/Mesh.h"
#include "MinusOne/ObjectsSystem/MinusOneObject.h"

namespace MinusOne {

	struct AlbedoInput
	{
		glm::vec3 Color = { 0.972f, 0.96f, 0.915f }; // Silver, from https://docs.unrealengine.com/en-us/Engine/Rendering/Materials/PhysicallyBased
		Ref<Texture2D> TextureMap;
		bool SRGB = true;
		bool UseTexture = false;
	};

	struct NormalInput
	{
		Ref<Texture2D> TextureMap;
		bool UseTexture = false;
	};

	struct MetalnessInput
	{
		float Value = 0.0f;
		Ref<Texture2D> TextureMap;
		bool UseTexture = false;
	};

	struct RoughnessInput
	{
		float Value = 1.0f;
		Ref<Texture2D> TextureMap;
		bool UseTexture = false;
	};

	class RenderComponent : public Component
	{
	public:
		RenderComponent(const std::string& shadername, const std::string& filename, MinusOneObject* obj)
		{
			m_MeshShader = Renderer::GetShaderLibrary()->Get(shadername);
			m_Material.reset(new MinusOne::Material(m_MeshShader));
			m_MeshMaterial.reset(new MinusOne::MaterialInstance(m_Material));
			m_Mesh.reset(new Mesh(filename, m_MeshShader, m_Material));
			m_Object = obj;
		}

		void Start() override;
		void OnUpdate(Timestep ts) override;
		void OnEnd() override;
		void OnImguiRender() override;

	private:
		Ref<Mesh> m_Mesh;
		MinusOneObject* m_Object;
		// Materials
		Ref<Shader> m_MeshShader;
		Ref<Material> m_Material;
		Ref<MaterialInstance> m_MeshMaterial;

		AlbedoInput m_AlbedoInput;

		NormalInput m_NormalInput;

		MetalnessInput m_MetalnessInput;

		RoughnessInput m_RoughnessInput;
	};

}