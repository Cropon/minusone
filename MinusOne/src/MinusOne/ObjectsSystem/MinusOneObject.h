#pragma once

#include "Transform.h"
#include "Component.h"
#include <vector>

namespace MinusOne {

	class MinusOneObject {

	public:
		MinusOneObject() = default;
		MinusOneObject(std::string& name) { m_Name.assign(name); }
		MinusOneObject(const char* name) { m_Name.assign(name); }
		~MinusOneObject() 
		{
			for each (auto component in m_Components)
			{
				delete component;
			}
		}

		void OnImGuiRender();
		void AddComponent(Component* component);
		void Start();
		void OnUpdate(Timestep ts);
		void OnEnd();
		
		void SetID(int id) { m_ID = id; }
		void SetName(std::string& name) { m_Name.assign(name); }
		void SetName(const char* name) { m_Name.assign(name); }

		Transform& GetTransform() { return m_ObjectTransform; }
		std::string& GetName() { return m_Name; }
		int GetID() { return m_ID; }
	private:
		int m_ID;
		std::string m_Name;
		std::vector<Component*> m_Components;
		Transform m_ObjectTransform;
	};

}