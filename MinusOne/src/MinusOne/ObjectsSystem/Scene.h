#pragma once

#include "MinusOneObject.h"
#include "MinusOne/Renderer/Camera.h"
#include "MinusOne/Renderer/Texture.h"

#include <vector>

namespace MinusOne {

	struct Light
	{
		glm::vec3 Direction;
		glm::vec3 Radiance;
	};

	class Scene 
	{
	public:
		Scene() :
			m_Camera(glm::perspectiveFov(glm::radians(45.0f), 1280.0f, 720.0f, 0.1f, 10000.0f)),
			m_Light()
		{
			m_CurrentlyActiveScene = this;

			m_Light.Direction = { -0.5f, -0.5f, 1.0f };
			m_Light.Radiance = { 1.0f, 1.0f, 1.0f };

			m_CheckerboardTex.reset(Texture2D::Create("assets/editor/Checkerboard.tga"));
		}

		~Scene() 
		{
			for each (auto obj in m_Objects)
			{
				delete obj;
			}
		}

		static Scene* GetCurrentActiveScene() { return m_CurrentlyActiveScene; }

		void PushObject(MinusOneObject* sceneObj);
		void Start();
		void OnUpdate(Timestep ts);
		void OnEnd();
		void OnImguiRender();

		Camera& GetCamera() { return m_Camera; }
		Light& GetLight() { return m_Light; }
		bool GetRadiancePrefilterUsage() { return false; }
		float GetEnvMapRotation() { return m_EnvMapRotation; }
		float GetExposure() { return m_Exposure; }
		Ref<Texture2D> GetLutTexture() { return m_BRDFLUT; }
		Ref<TextureCube> GetEnvironmentCubeMap() { return m_EnvironmentCubeMap; }
		Ref<TextureCube> GetEnvironmentIrradiance() { return m_EnvironmentIrradiance; }

#ifdef MINUSONE_EDITOR
		Ref<Texture2D> GetFallbackTexture() { return m_CheckerboardTex; }
		MinusOneObject* GetCurrentlySelectedObject() { return m_Objects[m_CurrentlyChosen]; }
#endif // MINUSONE_EDITOR
	private:
		static Scene* m_CurrentlyActiveScene;
		std::vector<MinusOneObject*> m_Objects;

		Ref<Texture2D> m_BRDFLUT;
		Camera m_Camera;
		Light m_Light;
		float m_LightMultiplier = 0.3f;

		// PBR params
		bool m_RadiancePrefilter = false;
		float m_EnvMapRotation = 0.0f;
		float m_Exposure = 1.0f;
		// Environment
		Ref<TextureCube> m_EnvironmentCubeMap, m_EnvironmentIrradiance;
		// Editor resources
#ifdef MINUSONE_EDITOR
		Ref<Texture2D> m_CheckerboardTex;
		int m_CurrentlyChosen = 0;
#endif
	};

}