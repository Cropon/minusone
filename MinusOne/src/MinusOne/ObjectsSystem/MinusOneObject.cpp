#include "mopch.h"
#include "MinusOneObject.h"
#include "imgui.h"

namespace MinusOne {

	void MinusOneObject::AddComponent(Component* component) 
	{
		if (component != nullptr)
			m_Components.push_back(component);
		else
		{
			MO_CORE_ERROR("ASSIGNING NULL COMPONENT");
		}
	}

	void MinusOneObject::Start() 
	{
		for each (auto component in m_Components)
		{
			component->Start();
		}
	}

	void MinusOneObject::OnUpdate(Timestep ts)
	{
		for each (auto component in m_Components)
		{
			component->OnUpdate(ts);
		}
	}

	void MinusOneObject::OnEnd()
	{
		for each (auto component in m_Components)
		{
			component->OnEnd();
		}
	}

	void MinusOneObject::OnImGuiRender() 
	{
		ImGui::Begin("Object");
		char* name = const_cast<char*>(m_Name.c_str());
		ImGui::InputText("##Object name", name, 50);
		SetName(name);
		m_ObjectTransform.OnImGuiRender();
		for each (auto component in m_Components)
		{
			component->OnImguiRender();
		}
		ImGui::End();
	}
}