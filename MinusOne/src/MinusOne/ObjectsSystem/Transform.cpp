#include "mopch.h"
#include "Transform.h"

#include <glm/gtx/euler_angles.hpp>
#include "imgui.h"

namespace MinusOne {

	Transform::Transform() 
	{
		m_Position = glm::vec3();
		m_Rotation = glm::vec3();
		m_Scale = glm::vec3(1, 1, 1);
	}

	void Transform::OnImGuiRender() 
	{
#ifdef MINUSONE_EDITOR
		if (ImGui::TreeNode("Transform"))
		{
			if (ImGui::CollapsingHeader("Position"))
			{
				ImGui::InputFloat3("Position##Position", &m_Position.x);
			}
			if (ImGui::CollapsingHeader("Rotation"))
			{
				ImGui::InputFloat3("Rotation##Rotation", &m_Rotation.x);
			}
			if (ImGui::CollapsingHeader("Scale"))
			{
				ImGui::InputFloat3("Scale##Scale", &m_Scale.x);
			}
			ImGui::TreePop();
		}
#endif // MINUSONE_EDITOR
	}

	glm::mat4 Transform::GetModelMatrix() 
	{
		glm::mat4 translation = glm::translate(glm::mat4(1), m_Position);
		glm::mat4 rotation = glm::orientate4(glm::vec3(glm::radians(m_Rotation.y), glm::radians(m_Rotation.x), 
			                                 glm::radians(m_Rotation.z)));
		glm::mat4 scale = glm::scale(glm::mat4(1), m_Scale);

		return translation * rotation * scale;
		/*
		auto val = translation * rotation * scale;
		MO_CORE_INFO("POSITION \n {0} {1} {2} {3} \n  {0} {1} {2} {3} \n  {0} {1} {2} {3} \n  {0} {1} {2} {3})", 
			val[0].x, val[0].y, val[0].z, val[0].w,
			val[1].x, val[1].y, val[1].z, val[1].w,
			val[2].x, val[2].y, val[2].z, val[2].w,
			val[3].x, val[3].y, val[3].z, val[3].w);

		return val;
		*/
	}
}