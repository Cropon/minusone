#pragma once

#define STB_IMAGE_IMPLEMENTATION
#include <memory>

#ifdef MO_PLATFORM_WINDOWS
    #ifdef MO_BUILD_DLL
        #define __declspec(dllexport)
    #else
        #define __declspec(dllimport)
    #endif

#else
    #error Windows support only!

#endif

#ifdef MO_ENABLE_ASSERTS
#define MO_ASSERT(x, ...) { if(!(x)) { HZ_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#define MO_CORE_ASSERT(x, ...) { if(!(x)) { HZ_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#else
#define MO_ASSERT(x, ...)
#define MO_CORE_ASSERT(x, ...)
#endif

#define BIT(x) (1 << x)
#define MO_BIND_EVENT_FN(fn) std::bind(&fn, this, std::placeholders::_1)

namespace MinusOne {

	template<typename T>
	using Scope = std::unique_ptr<T>;

	template<typename T>
	using Ref = std::shared_ptr<T>;

	using byte = unsigned char;
}