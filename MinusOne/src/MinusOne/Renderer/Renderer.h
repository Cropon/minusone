#pragma once

#include "RenderCommandQueue.h"
#include "RendererAPI.h"

namespace MinusOne {
	class ShaderLibrary;

	// TODO: Maybe this should be renamed to RendererAPI? Because we want an actual renderer vs API calls...
	class Renderer
	{
	public:
		typedef void(*RenderCommandFn)(void*);

		// Commands
		static void Clear();
		static void Clear(float r, float g, float b, float a = 1.0f);
		static void SetClearColor(float r, float g, float b, float a);

		static void DrawIndexed(uint32_t count, bool depthTest = true);

		static void ClearMagenta();

		static void Init();

		static const Scope<ShaderLibrary>& GetShaderLibrary() { return Get().m_ShaderLibrary; }

		static void* Submit(RenderCommandFn fn, unsigned int size)
		{
			return s_Instance->m_CommandQueue.Allocate(fn, size);
		}

		void WaitAndRender();

		inline static Renderer& Get() { return *s_Instance; }
	private:
		static Renderer* s_Instance;

		RenderCommandQueue m_CommandQueue;
		Scope<ShaderLibrary> m_ShaderLibrary;
	};
}

#define MO_RENDER_PASTE2(a, b) a ## b
#define MO_RENDER_PASTE(a, b) MO_RENDER_PASTE2(a, b)
#define MO_RENDER_UNIQUE(x) MO_RENDER_PASTE(x, __LINE__)

#define MO_RENDER(code) \
    struct MO_RENDER_UNIQUE(MORenderCommand) \
    {\
        static void Execute(void*)\
        {\
            code\
        }\
    };\
	{\
		auto mem = ::MinusOne::Renderer::Submit(MO_RENDER_UNIQUE(MORenderCommand)::Execute, sizeof(MO_RENDER_UNIQUE(MORenderCommand)));\
		new (mem) MO_RENDER_UNIQUE(MORenderCommand)();\
	}\

#define MO_RENDER_1(arg0, code) \
	do {\
    struct MO_RENDER_UNIQUE(MORenderCommand) \
    {\
		MO_RENDER_UNIQUE(MORenderCommand)(typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0) \
		: arg0(arg0) {}\
		\
        static void Execute(void* argBuffer)\
        {\
			auto& arg0 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg0;\
            code\
        }\
		\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0;\
    };\
	{\
		auto mem = ::MinusOne::Renderer::Submit(MO_RENDER_UNIQUE(MORenderCommand)::Execute, sizeof(MO_RENDER_UNIQUE(MORenderCommand)));\
		new (mem) MO_RENDER_UNIQUE(MORenderCommand)(arg0);\
	} } while(0)

#define MO_RENDER_2(arg0, arg1, code) \
    struct MO_RENDER_UNIQUE(MORenderCommand) \
    {\
		MO_RENDER_UNIQUE(MORenderCommand)(typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1) \
		: arg0(arg0), arg1(arg1) {}\
		\
        static void Execute(void* argBuffer)\
        {\
			auto& arg0 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg0;\
			auto& arg1 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg1;\
            code\
        }\
		\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1;\
    };\
	{\
		auto mem = ::MinusOne::Renderer::Submit(MO_RENDER_UNIQUE(MORenderCommand)::Execute, sizeof(MO_RENDER_UNIQUE(MORenderCommand)));\
		new (mem) MO_RENDER_UNIQUE(MORenderCommand)(arg0, arg1);\
	}\

#define MO_RENDER_3(arg0, arg1, arg2, code) \
    struct MO_RENDER_UNIQUE(MORenderCommand) \
    {\
		MO_RENDER_UNIQUE(MORenderCommand)(typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg2)>::type>::type arg2) \
		: arg0(arg0), arg1(arg1), arg2(arg2) {}\
		\
        static void Execute(void* argBuffer)\
        {\
			auto& arg0 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg0;\
			auto& arg1 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg1;\
			auto& arg2 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg2;\
            code\
        }\
		\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg2)>::type>::type arg2;\
    };\
	{\
		auto mem = ::MinusOne::Renderer::Submit(MO_RENDER_UNIQUE(MORenderCommand)::Execute, sizeof(MO_RENDER_UNIQUE(MORenderCommand)));\
		new (mem) MO_RENDER_UNIQUE(MORenderCommand)(arg0, arg1, arg2);\
	}\

#define MO_RENDER_4(arg0, arg1, arg2, arg3, code) \
    struct MO_RENDER_UNIQUE(MORenderCommand) \
    {\
		MO_RENDER_UNIQUE(MORenderCommand)(typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg2)>::type>::type arg2,\
											typename ::std::remove_const<typename ::std::remove_reference<decltype(arg3)>::type>::type arg3)\
		: arg0(arg0), arg1(arg1), arg2(arg2), arg3(arg3) {}\
		\
        static void Execute(void* argBuffer)\
        {\
			auto& arg0 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg0;\
			auto& arg1 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg1;\
			auto& arg2 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg2;\
			auto& arg3 = ((MO_RENDER_UNIQUE(MORenderCommand)*)argBuffer)->arg3;\
            code\
        }\
		\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg0)>::type>::type arg0;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg1)>::type>::type arg1;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg2)>::type>::type arg2;\
		typename ::std::remove_const<typename ::std::remove_reference<decltype(arg3)>::type>::type arg3;\
    };\
	{\
		auto mem = Renderer::Submit(MO_RENDER_UNIQUE(MORenderCommand)::Execute, sizeof(MO_RENDER_UNIQUE(MORenderCommand)));\
		new (mem) MO_RENDER_UNIQUE(MORenderCommand)(arg0, arg1, arg2, arg3);\
	}

#define MO_RENDER_S(code) auto self = this;\
	MO_RENDER_1(self, code)

#define MO_RENDER_S1(arg0, code) auto self = this;\
	MO_RENDER_2(self, arg0, code)

#define MO_RENDER_S2(arg0, arg1, code) auto self = this;\
	MO_RENDER_3(self, arg0, arg1, code)

#define MO_RENDER_S3(arg0, arg1, arg2, code) auto self = this;\
	MO_RENDER_4(self, arg0, arg1, arg2, code)