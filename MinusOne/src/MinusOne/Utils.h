#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace MinusOne {

	struct Buffer
	{
		byte* Data;
		uint32_t Size;

		Buffer()
			: Data(nullptr), Size(0)
		{
		}

		Buffer(byte* data, uint32_t size)
			: Data(data), Size(size)
		{
		}

		static Buffer Copy(void* data, uint32_t size)
		{
			Buffer buffer;
			buffer.Allocate(size);
			memcpy(buffer.Data, data, size);
			return buffer;
		}

		void Allocate(uint32_t size)
		{
			delete[] Data;
			Data = nullptr;

			if (size == 0)
				return;

			Data = new byte[size];
			Size = size;
		}

		void ZeroInitialize()
		{
			if (Data)
				memset(Data, 0, Size);
		}

		void Write(byte* data, uint32_t size, uint32_t offset = 0)
		{
			MO_CORE_ASSERT(offset + size <= Size, "Buffer overflow!");
			memcpy(Data + offset, data, size);
		}

		operator bool() const
		{
			return Data;
		}

		byte& operator[](int index)
		{
			return Data[index];
		}

		byte operator[](int index) const
		{
			return Data[index];
		}

		template<typename T>
		T* As()
		{
			return (T*)Data;
		}

		inline uint32_t GetSize() const { return Size; }
	};

#pragma region ImGui UI Helpers
    enum class PropertyFlag
	{
		None = 0, ColorProperty = 1
	};

	class ImGuiHelper 
	{
	public:

		static void Property(const std::string& name, bool& value);
		static void Property(const std::string& name, float& value, float min, float max, PropertyFlag flags);
		static void Property(const std::string& name, glm::vec3& value, PropertyFlag flags);
		static void Property(const std::string& name, glm::vec3& value, float min, float max, PropertyFlag flags);
		static void Property(const std::string& name, glm::vec4& value, PropertyFlag flags);
		static void Property(const std::string& name, glm::vec4& value, float min, float max, PropertyFlag flags);

	};

#pragma endregion
}