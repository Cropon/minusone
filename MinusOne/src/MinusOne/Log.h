#pragma once
#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

namespace MinusOne {
	class Log
	{
	public:
		static void Init();

		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; }
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; }

	private:
		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
	};
}

//Core log macros
#define MO_CORE_FATAL(...)    ::MinusOne::Log::GetCoreLogger()->critical(__VA_ARGS__)
#define MO_CORE_ERROR(...)    ::MinusOne::Log::GetCoreLogger()->error(__VA_ARGS__)
#define MO_CORE_WARN(...)     ::MinusOne::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define MO_CORE_INFO(...)     ::MinusOne::Log::GetCoreLogger()->info(__VA_ARGS__)
#define MO_CORE_TRACE(...)    ::MinusOne::Log::GetCoreLogger()->trace(__VA_ARGS__)

//Client log macros
#define MO_FATAL(...)    ::MinusOne::Log::GetClientLogger()->critical(__VA_ARGS__)
#define MO_ERROR(...)    ::MinusOne::Log::GetClientLogger()->error(__VA_ARGS__)
#define MO_WARN(...)     ::MinusOne::Log::GetClientLogger()->warn(__VA_ARGS__)
#define MO_INFO(...)     ::MinusOne::Log::GetClientLogger()->info(__VA_ARGS__)
#define MO_TRACE(...)    ::MinusOne::Log::GetClientLogger()->trace(__VA_ARGS__)