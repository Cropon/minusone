#pragma once
#include "MinusOne/Layer.h"
#include "MinusOne/Renderer/Mesh.h"
#include "MinusOne/Renderer/Shader.h"
#include "MinusOne/ObjectsSystem/Scene.h"
#include "MinusOne/Events/MouseEvent.h"
#include "MinusOne/Events/ApplicationEvent.h"
#include <string>

namespace MinusOne {

	class GizmoLayer : public Layer 
	{
	public:
		GizmoLayer();
		~GizmoLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnImGuiRender() override;
		virtual void OnUpdate(Timestep ts) override;
		virtual void OnEvent(Event& event) override;
		void SetGuiWindowOffset(glm::vec2 offset) { m_GuiWindowOffset = offset; }
		void SetWindowSize(glm::vec2 size) { m_WindowSize = size; }
	private:
		bool OnMouseMoved(MouseMovedEvent& e);
		bool OnMouseDown(MouseButtonPressedEvent& e);
		bool OnMouseUp(MouseButtonReleasedEvent& e);
		bool OnWindowMoved(WindowMovedEvent& e);
		glm::vec3 GetMovingAxisPosition(int axis); //x = 0, y = 1, z = 2

		glm::vec3 m_ObjectsStartingPosition;
		float m_ObjectsStartingOffset;
		bool m_MovingX = false;
		bool m_MovingY = false;
		bool m_MovingZ = false;
		glm::mat4x4 GetTransformForArrow(glm::vec3 direction);
		glm::vec2 m_WindowOffset;
		glm::vec2 m_GuiWindowOffset;
		glm::vec2 m_WindowSize;
		glm::vec2 m_LastMousePos;
		Ref<Mesh> m_ArrowX, m_ArrowY, m_ArrowZ;
		Ref<Shader> m_ColorShader;
		Ref<MaterialInstance> m_GizmoMaterialX, m_GizmoMaterialY, m_GizmoMaterialZ;
		Scene* m_Scene;
	};
	
}