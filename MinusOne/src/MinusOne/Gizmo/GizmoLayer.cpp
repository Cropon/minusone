﻿#include "mopch.h"
#include "GizmoLayer.h"

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/intersect.hpp>
#include "MinusOne/Math/AABB.h"
#include "MinusOne/Math/Ray.h"
#include "MinusOne/ObjectsSystem/RenderComponent.h"
#include "MinusOne/Application.h"
#include "MinusOne/Input.h"
#include "MinusOne/KeyCodes.h"

namespace MinusOne {

	GizmoLayer::GizmoLayer() : Layer("GizmoLayer"){}

	void GizmoLayer::OnAttach() 
	{
		m_ColorShader = Shader::Create("assets/shaders/MinusOneSimpleColor.glsl");
		m_GizmoMaterialX = MaterialInstance::Create(Material::Create(m_ColorShader));
		m_GizmoMaterialY = MaterialInstance::Create(Material::Create(m_ColorShader));
		m_GizmoMaterialZ = MaterialInstance::Create(Material::Create(m_ColorShader));

		m_ArrowX.reset(new Mesh("assets/models/arrow.fbx", m_ColorShader, m_GizmoMaterialX->GetMaterial()));
		m_ArrowY.reset(new Mesh("assets/models/arrow.fbx", m_ColorShader, m_GizmoMaterialY->GetMaterial()));
		m_ArrowZ.reset(new Mesh("assets/models/arrow.fbx", m_ColorShader, m_GizmoMaterialZ->GetMaterial()));
		m_Scene = Scene::GetCurrentActiveScene();
		m_WindowOffset = glm::vec2(Application::Get().GetWindow().GetPositionX(), Application::Get().GetWindow().GetPositionY());
	}

	void GizmoLayer::OnDetach(){}
	void GizmoLayer::OnUpdate(Timestep ts) 
	{
		auto viewProjection = m_Scene->GetCamera().GetProjectionMatrix() * m_Scene->GetCamera().GetViewMatrix();

		m_ColorShader->Bind();
		m_GizmoMaterialX->Set("u_ViewProjectionMatrix", m_Scene->GetCamera().GetViewProjectionMatrix());
		m_GizmoMaterialY->Set("u_ViewProjectionMatrix", m_Scene->GetCamera().GetViewProjectionMatrix());
		m_GizmoMaterialZ->Set("u_ViewProjectionMatrix", m_Scene->GetCamera().GetViewProjectionMatrix());
		m_GizmoMaterialX->Set("u_ModelMatrix", GetTransformForArrow(glm::vec3(0, 0, 90)));
		m_GizmoMaterialY->Set("u_ModelMatrix", GetTransformForArrow(glm::vec3(0, -90, 0)));
		m_GizmoMaterialZ->Set("u_ModelMatrix", GetTransformForArrow(glm::vec3(90, 0, 0)));
		m_GizmoMaterialX->Set("u_AlbedoColor", glm::vec3(1, 0, 0));
		m_GizmoMaterialY->Set("u_AlbedoColor", glm::vec3(0, 1, 0));
		m_GizmoMaterialZ->Set("u_AlbedoColor", glm::vec3(0, 0, 1));

		m_ArrowX->Render(ts, glm::scale(glm::mat4(1.0f), glm::vec3(1)), m_GizmoMaterialX);
		m_ArrowY->Render(ts, glm::scale(glm::mat4(1.0f), glm::vec3(1)), m_GizmoMaterialY);
		m_ArrowZ->Render(ts, glm::scale(glm::mat4(1.0f), glm::vec3(1)), m_GizmoMaterialZ);
	}

	glm::mat4x4 GizmoLayer::GetTransformForArrow(glm::vec3 direction) 
	{
		glm::mat4 translation = glm::translate(glm::mat4(1), m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition());
		glm::mat4 rotation = glm::orientate4(glm::vec3(glm::radians(direction.y), glm::radians(direction.x),
			glm::radians(direction.z)));
		glm::mat4 scale = glm::scale(glm::mat4(1), glm::vec3(1, 1, 1));

		return translation * rotation * scale;
	}

	void GizmoLayer::OnEvent(Event& event)
	{
		EventDispatcher dispatcher(event);
		dispatcher.Dispatch<MouseMovedEvent>(MO_BIND_EVENT_FN(GizmoLayer::OnMouseMoved));
		dispatcher.Dispatch<MouseButtonPressedEvent>(MO_BIND_EVENT_FN(GizmoLayer::OnMouseDown));
		dispatcher.Dispatch<MouseButtonReleasedEvent>(MO_BIND_EVENT_FN(GizmoLayer::OnMouseUp));
		dispatcher.Dispatch<WindowMovedEvent>(MO_BIND_EVENT_FN(GizmoLayer::OnWindowMoved));
	}
	void GizmoLayer::OnImGuiRender(){}

	float lerp(float a, float b, float t) {
		return a + t * (b - a);
	}
	bool GizmoLayer::OnMouseDown(MouseButtonPressedEvent& e)
	{
		if (Input::IsKeyPressed(MO_KEY_LEFT_ALT)) return false;
		glm::vec3 mousePos = m_Scene->GetCamera().GetPosition();
		glm::vec3 ray = m_Scene->GetCamera().ViewCoordToRay(m_LastMousePos);
		glm::vec3 min = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(0.0f, -0.2f, -0.2f);
		glm::vec3 max = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(1.0f, 0.2f, 0.2f);
		AABB aabbX = AABB(min, max);
		min = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(-0.2f, 0.0f, -0.2f);
	    max = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(0.2f, 1.0f, 0.2f);
		AABB aabbY = AABB(min, max);
		min = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(-0.2f, -0.2f, 0.0f);
		max = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition() + glm::vec3(0.2f, 0.2f, 1.0f);
		AABB aabbZ = AABB(min, max);
		glm::vec3 rayVector = glm::vec3(ray.x, ray.y, ray.z);
		Ray rayR(mousePos, rayVector);
		if (Ray::IsIntersection(rayR, aabbX.GetBounds()))
		{
			m_MovingX = true;
			m_ObjectsStartingPosition = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition();
			m_ObjectsStartingOffset = m_ObjectsStartingPosition.x - GetMovingAxisPosition(0).x;
			return false;
		}
		if (Ray::IsIntersection(rayR, aabbY.GetBounds()))
		{
			m_MovingY = true;
			m_ObjectsStartingPosition = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition();
			m_ObjectsStartingOffset = m_ObjectsStartingPosition.y - GetMovingAxisPosition(1).y;
			return false;
		}
		if (Ray::IsIntersection(rayR, aabbZ.GetBounds()))
		{
			m_MovingZ = true;
			m_ObjectsStartingPosition = m_Scene->GetCurrentlySelectedObject()->GetTransform().GetPosition();
			m_ObjectsStartingOffset = m_ObjectsStartingPosition.z - GetMovingAxisPosition(2).z;
			return false;
		}
		
		return false;
	}

	bool GizmoLayer::OnMouseUp(MouseButtonReleasedEvent& e)
	{
		m_MovingX = false;
		m_MovingY = false;
		m_MovingZ = false;
		return false;
	}

	bool GizmoLayer::OnMouseMoved(MouseMovedEvent& e) 
	{
		m_LastMousePos = glm::vec2(e.GetX(), e.GetY());
		m_LastMousePos -= (m_GuiWindowOffset - m_WindowOffset);
		if (m_MovingX) 
		{
			glm::vec3 toSet = glm::vec3(GetMovingAxisPosition(0).x + m_ObjectsStartingOffset, m_ObjectsStartingPosition.y, m_ObjectsStartingPosition.z);
			m_Scene->GetCurrentlySelectedObject()->GetTransform().SetPosition(toSet);
			return false;
		}
		if (m_MovingY)
		{
			glm::vec3 toSet = glm::vec3(m_ObjectsStartingPosition.x, GetMovingAxisPosition(1).y + m_ObjectsStartingOffset, m_ObjectsStartingPosition.z);

			m_Scene->GetCurrentlySelectedObject()->GetTransform().SetPosition(toSet);
			return false;
		}
		if (m_MovingZ)
		{
			glm::vec3 toSet = glm::vec3(m_ObjectsStartingPosition.x, m_ObjectsStartingPosition.y, GetMovingAxisPosition(2).z + m_ObjectsStartingOffset);
			m_Scene->GetCurrentlySelectedObject()->GetTransform().SetPosition(toSet);
			return false;
		}
		return false;
	}

	glm::vec3 GizmoLayer::GetMovingAxisPosition(int axis)
	{
		glm::vec3 orig = m_Scene->GetCamera().GetPosition();
		glm::vec3 ray = m_Scene->GetCamera().ViewCoordToRay(m_LastMousePos) * 100.0f;
		glm::vec3 normal;
		switch (axis)
		{
		default:
			normal = glm::vec3(0.0f, 1.0f, 0.0f);
			break;
		case 1:
			normal = glm::vec3(1.0f, 0.0f, 0.0f);
			break;
		case 2:
			normal = glm::vec3(0.0f, 1.0f, 0.0f);
			break;
		}
		float distance;
		bool intersect = glm::intersectRayPlane(orig, ray, m_ObjectsStartingPosition, normal, distance);
		if (intersect)
		{
			return orig + distance * ray;
		}
		else
		{
			switch (axis)
			{
			default:
				normal = glm::vec3(0.0f, -1.0f, 0.0f);
				break;
			case 1:
				normal = glm::vec3(-1.0f, 0.0f, 0.0f);
				break;
			case 2:
				normal = glm::vec3(0.0f, -1.0f, 0.0f);
				break;
			}
			intersect = glm::intersectRayPlane(orig, ray, m_ObjectsStartingPosition, normal, distance);
			if (intersect)
			{
				return orig + distance * ray;
			}
			return glm::vec3(FLT_MIN, FLT_MIN, FLT_MIN);
		}
	}

	bool GizmoLayer::OnWindowMoved(WindowMovedEvent& e) 
	{
		m_WindowOffset = glm::vec2(e.GetX(), e.GetY());
		return false;
	}
}