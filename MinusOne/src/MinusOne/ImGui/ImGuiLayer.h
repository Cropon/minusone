#pragma once

#include "MinusOne/Layer.h"
#include "MinusOne/Events/KeyEvent.h"
#include "MinusOne/Events/MouseEvent.h"
#include "MinusOne/Events/ApplicationEvent.h"

namespace MinusOne {

	class ImGuiLayer : public Layer 
	{
	public:
		ImGuiLayer();
		~ImGuiLayer() = default;

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnImGuiRender() override;

		void Begin();
		void End();
	private:

		float m_Time = 0.0f;
	};

}

