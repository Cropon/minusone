#pragma once
#include <glm/glm.hpp>

namespace MinusOne {
	class Ray
	{
	public:
		Ray(const glm::vec3& orig, const glm::vec3& dir) : m_Orig(orig), m_Dir(dir)
		{
			m_Invdir = 1.0f / dir;
			m_Sign[0] = (m_Invdir.x < 0);
			m_Sign[1] = (m_Invdir.y < 0);
			m_Sign[2] = (m_Invdir.z < 0);
		}

		static bool IsIntersection(const Ray& r, glm::vec3* bounds)
		{
			float tmin, tmax, tymin, tymax, tzmin, tzmax;

			tmin = (bounds[r.m_Sign[0]].x - r.m_Orig.x) * r.m_Invdir.x;
			tmax = (bounds[1 - r.m_Sign[0]].x - r.m_Orig.x) * r.m_Invdir.x;
			tymin = (bounds[r.m_Sign[1]].y - r.m_Orig.y) * r.m_Invdir.y;
			tymax = (bounds[1 - r.m_Sign[1]].y - r.m_Orig.y) * r.m_Invdir.y;

			if ((tmin > tymax) || (tymin > tmax))
				return false;
			if (tymin > tmin)
				tmin = tymin;
			if (tymax < tmax)
				tmax = tymax;

			tzmin = (bounds[r.m_Sign[2]].z - r.m_Orig.z) * r.m_Invdir.z;
			tzmax = (bounds[1 - r.m_Sign[2]].z - r.m_Orig.z) * r.m_Invdir.z;

			if ((tmin > tzmax) || (tzmin > tmax))
				return false;
			if (tzmin > tmin)
				tmin = tzmin;
			if (tzmax < tmax)
				tmax = tzmax;

			return true;
		}

	private:
		glm::vec3 m_Orig, m_Dir;
		glm::vec3 m_Invdir;
		int m_Sign[3];
	};
}