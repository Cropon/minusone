#pragma once
#include <glm/glm.hpp>
namespace MinusOne {
	class AABB
	{
	public:
		AABB(const glm::vec3& vmin, const glm::vec3& vmax)
		{
			m_Bounds[0] = vmin;
			m_Bounds[1] = vmax;
		}

		glm::vec3* GetBounds() 
		{
			return m_Bounds;
		}
	private:
		glm::vec3 m_Bounds[2];
	};
}