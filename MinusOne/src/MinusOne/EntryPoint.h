#pragma once

#ifdef MO_PLATFORM_WINDOWS

extern MinusOne::Application* MinusOne::CreateApplication();

int main(int argc, char** argv) 
{
#ifdef MO_DIST
	FreeConsole();
#endif 
	MinusOne::Log::Init();
	MO_CORE_INFO("Initialized Log!");
	auto app = MinusOne::CreateApplication();
	app->Run();
	delete app;
}

#endif
