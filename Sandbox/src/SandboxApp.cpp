#include <MinusOne.h>

#include "Platform/OpenGL/OpenGLShader.h"

#include "imgui/imgui.h"
#include "EditorLayer.h"

class ExampleLayer : public MinusOne::Layer
{
public:
	ExampleLayer()
	{
	}

	void OnUpdate(MinusOne::Timestep ts) override
	{
	}

	virtual void OnImGuiRender() override
	{
	}

	void OnEvent(MinusOne::Event& event) override
	{
	}
private:
};



class MinusOneInutApplication : public MinusOne::Application
{
public:
	MinusOneInutApplication()
	{
		PushLayer(new MinusOne::EditorLayer());
	}

};

MinusOne::Application* MinusOne::CreateApplication()
{
	return new MinusOneInutApplication();
}

int main(int argc, char** argv)
{
#ifdef MO_DIST
	FreeConsole();
#endif 
	MinusOne::Log::Init();
	MO_CORE_INFO("Initialized Log!");
	auto app = MinusOne::CreateApplication();
	app->Run();
	delete app;
}
