#pragma once

#include "MinusOne.h"

#include "MinusOne/ImGui/ImGuiLayer.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

#include <string>
namespace MinusOne {

	class EditorLayer : public Layer
	{
	public:
		EditorLayer();
		virtual ~EditorLayer();

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnUpdate(Timestep ts) override;

		virtual void OnImGuiRender() override;
		virtual void OnEvent(Event& event) override;

	private:
		Ref<GizmoLayer> m_GizmoLayer;
		Ref<Shader> m_QuadShader;
		Ref<Shader> m_HDRShader;
		Ref<Shader> m_GridShader;
		Ref<Mesh> m_PlaneMesh;

		Ref<MaterialInstance> m_GridMaterial;

		float m_GridScale = 16.025f, m_GridSize = 0.025f;
		float m_MeshScale = 1.0f;

		std::unique_ptr<Framebuffer> m_Framebuffer, m_FinalPresentBuffer;

		Ref<VertexArray> m_FullscreenQuadVertexArray;

		Scene m_Scene;
	};

}
