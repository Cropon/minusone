workspace "MinusOne"
	architecture "x64"
	startproject "MinusOneInput"

	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "MinusOne/vendor/GLFW/include"
IncludeDir["glad"] = "MinusOne/vendor/glad/include"
IncludeDir["ImGui"] = "MinusOne/vendor/imgui"
IncludeDir["glm"] = "MinusOne/vendor/glm"
IncludeDir["stb"] = "MinusOne/vendor/stb"

group "Dependencies"
	include "MinusOne/vendor/GLFW"
	include "MinusOne/vendor/glad"
	include "MinusOne/vendor/imgui"

group ""

project "MinusOne"
	location "MinusOne"
	kind "StaticLib"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-intermediate/" .. outputdir .. "/%{prj.name}")

	pchheader "mopch.h"
	pchsource "MinusOne/src/mopch.cpp"

	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp",
		"%{prj.name}/src/**.c",
		"%{prj.name}/vendor/stb/**.h",
		"%{prj.name}/vendor/glm/glm/**.hpp",
		"%{prj.name}/vendor/glm/glm/**.inl",
	}

	defines
	{
		"_CRT_SECURE_NO_WARNINGS",
		"MINUSONE_EDITOR"
	}

	includedirs 
	{
		"%{prj.name}/src",
		"%{prj.name}/vendor/spdlog/include",
		"%{IncludeDir.GLFW}",
		"%{IncludeDir.glad}",
		"%{IncludeDir.ImGui}",
		"%{IncludeDir.glm}",
        "%{prj.name}/vendor/assimp/include",
		"%{IncludeDir.stb}"
	}

	links
	{
		"GLFW",
		"glad",
		"ImGui",
		"opengl32.lib"
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{
			"MO_PLATFORM_WINDOWS",
			"MO_BUILD_DLL",
			"GLFW_INCLUDE_NONE"
		}

	filter "configurations:Debug"
		defines "MO_DEBUG"
		runtime "Debug"
		symbols "on"
		
	filter "configurations:Release"
		defines "MO_RELEASE"
		runtime "Release"
		optimize "on"

	filter "configurations:Dist"
		defines "MO_DIST"
		runtime "Release"
		optimize "on"

project "MinusOneInput"
	location "MinusOneInput"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-intermediate/" .. outputdir .. "/%{prj.name}")
	
	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"MinusOne/vendor/spdlog/include",
		"MinusOne/src",
		"MinusOne/vendor",
		"%{IncludeDir.glm}"
	}

	links
	{
		"MinusOne",
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{
			"MO_PLATFORM_WINDOWS"
		}

	filter "configurations:Debug"
		defines "MO_DEBUG"
		runtime "Debug"
		symbols "on"
		
		links
		{
			"MinusOne/vendor/assimp/bin/Debug/assimp-vc141-mtd.lib"
		}

	filter "configurations:Release"
		defines "MO_RELEASE"
		runtime "Release"
		optimize "on"

		links
		{
			"MinusOne/vendor/assimp/bin/Release/assimp-vc141-mt.lib"
		}

	filter "configurations:Dist"
		defines "MO_DIST"
		runtime "Release"
		optimize "on"

		links
		{
			"MinusOne/vendor/assimp/bin/Release/assimp-vc141-mt.lib"
		}

project "Sandbox"
	location "Sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-intermediate/" .. outputdir .. "/%{prj.name}")
	
	files
	{
		"%{prj.name}/src/**.h",
		"%{prj.name}/src/**.cpp"
	}

	includedirs
	{
		"MinusOne/vendor/spdlog/include",
		"MinusOne/src",
		"MinusOne/vendor",
		"%{IncludeDir.glm}"
	}

	links
	{
		"MinusOne"
	}

	filter "system:windows"
		systemversion "latest"

		defines
		{
			"MO_PLATFORM_WINDOWS",
			"MINUSONE_EDITOR"
		}

	filter "configurations:Debug"
		defines "MO_DEBUG"
		runtime "Debug"
		symbols "on"
		
		links
		{
			"MinusOne/vendor/assimp/bin/Debug/assimp-vc141-mt.lib"
		}

	filter "configurations:Release"
		defines "MO_RELEASE"
		runtime "Release"
		optimize "on"

		links
		{
			"MinusOne/vendor/assimp/bin/Release/assimp-vc141-mt.lib"
		}

	filter "configurations:Dist"
		defines "MO_DIST"
		runtime "Release"
		optimize "on"
		
		links
		{
			"MinusOne/vendor/assimp/bin/Release/assimp-vc141-mt.lib"
		}